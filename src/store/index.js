/*
 * Import config store by react
 * **************************** */
import { createStore, combineReducers, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";

/*
 * Reducers
 * ******** */
import { StatReducer } from "./reducers/StatReducer";
import { TitreReducer } from "./reducers/TitreReducer";
import { PartieReducer } from "./reducers/PartieReducer";
import { SousPartieReducer } from "./reducers/SousPartieReducer";
import { JointuresReducer } from "./reducers/JointuresReducer";
import { MailingReducer } from "./reducers/MailReducer";

/*
 * Centralisation du store (root reducers)
 * *************************************** */
const rootReducer = combineReducers({
    stat: StatReducer,
    titre: TitreReducer,
    partie: PartieReducer,
    sousPartie: SousPartieReducer,
    jointures: JointuresReducer,
    mailing: MailingReducer
});

/*
 * Export du store
 * *************** */
export const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk))); //Dev
// export const store = createStore(rootReducer); //prod
