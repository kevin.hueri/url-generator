/*
 * Import Actions {...}
 * ******************** */
import * as Actions from "../actions/ActionTypes.js";

/*
 * Selector
 * ******** */
const initialState = {
    dataTitreParties: [],
    dataPartieSousParties: [],
};

/*
 * Reducers
 * ******** */

export function JointuresReducer(state = initialState, action) {
    switch (action.type) {
        default:
            return state;
        case Actions.GET_JOINTURESTITREPARTIE_DATA:
            return { ...state, dataTitreParties: action.payload };
        case Actions.GETONE_JOINTURESTITREPARTIE_DATA:
            return { ...state, dataTitreParties: action.payload };
        case Actions.GET_JOINTURESPARTIESOUSPARTIE_DATA:
            return { ...state, dataPartieSousParties: action.payload };
        case Actions.GETONE_JOINTURESPARTIESOUSPARTIE_DATA:
            return { ...state, dataPartieSousParties: action.payload };
    }
}

/*
 * Getters
 * ******* */
