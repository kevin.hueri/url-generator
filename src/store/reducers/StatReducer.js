/*
 * Import Actions {...}
 * ******************** */
import * as Actions from "../actions/ActionTypes.js";

/*
 * Selector
 * ******** */
const initialState = {
  dataImmanquablesTitle: {},
  dataCampusEtMoiTitle: {},
  dataImmanquables: {},
  dataCampusEtMoi: {},
  dataCourbes: {}
};

/*
 * Reducers
 * ******** */

export function StatReducer(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
    case Actions.GET_TITLEIMMANQUABLESSTAT_DATA:
      return { ...state, dataImmanquablesTitle: action.payload };
    case Actions.GET_TITLECAMPUSETMOITAT_DATA:
      return { ...state, dataCampusEtMoiTitle: action.payload };
    case Actions.GET_STATCAMPUSETMOI_DATA:
      return { ...state, dataCampusEtMoi: action.payload };
    case Actions.GET_STATIMMANQUABLES_DATA:
      return { ...state, dataImmanquables: action.payload };
    case Actions.GET_STATCOURBES_DATA:
      return { ...state, dataCourbes: action.payload };
  }
}

/*
 * Getters
 * ******* */
