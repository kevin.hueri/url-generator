/*
 * Import Actions {...}
 * ******************** */
import * as Actions from "../actions/ActionTypes.js";

/*
 * Selector
 * ******** */
const initialState = {
    dataTitre: [],
};

/*
 * Reducers
 * ******** */

export function TitreReducer(state = initialState, action) {
    switch (action.type) {
        default:
            return state;
        case Actions.GET_TITRE_DATA:
            return { ...state, dataTitre: action.payload };
        case Actions.GETONE_TITRE_DATA:
            return { ...state, dataTitre: action.payload };
        case Actions.POST_TITRE_DATA:
            return { ...state, dataTitre: action.payload };
        case Actions.PUT_TITRE_DATA:
            return { ...state, dataTitre: action.payload };
        case Actions.DELETEONE_TITRE_DATA:
            return { ...state, dataTitre: action.payload };
        case Actions.DELETEALL_TITRE_DATA:
            return { ...state, dataTitre: action.payload };
    }
}

/*
 * Getters
 * ******* */
