/*
 * Import Actions {...}
 * ******************** */
import * as Actions from "../actions/ActionTypes.js";

/*
 * Selector
 * ******** */
const initialState = {
    dataMailing: [],
};

/*
 * Reducers
 * ******** */

export function MailingReducer(state = initialState, action) {
    switch (action.type) {
        default:
            return state;
        case Actions.POST_PARTIE_DATA:
            return { ...state, dataMailing: action.payload };
    }
}

/*
 * Getters
 * ******* */
