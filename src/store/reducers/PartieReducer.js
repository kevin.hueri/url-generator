/*
 * Import Actions {...}
 * ******************** */
import * as Actions from "../actions/ActionTypes.js";

/*
 * Selector
 * ******** */
const initialState = {
    dataPartie: [],
};

/*
 * Reducers
 * ******** */

export function PartieReducer(state = initialState, action) {
    switch (action.type) {
        default:
            return state;
        case Actions.GET_PARTIE_DATA:
            return { ...state, dataPartie: action.payload };
        case Actions.GETONE_PARTIE_DATA:
            return { ...state, dataPartie: action.payload };
        case Actions.POST_PARTIE_DATA:
            return { ...state, dataPartie: action.payload };
        case Actions.PUT_PARTIE_DATA:
            return { ...state, dataPartie: action.payload };
        case Actions.DELETEONE_PARTIE_DATA:
            return { ...state, dataPartie: action.payload };
        case Actions.DELETEALL_PARTIE_DATA:
            return { ...state, dataPartie: action.payload };
    }
}

/*
 * Getters
 * ******* */
