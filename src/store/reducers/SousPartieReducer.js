/*
 * Import Actions {...}
 * ******************** */
import * as Actions from "../actions/ActionTypes.js";

/*
 * Selector
 * ******** */
const initialState = {
    dataSousPartie: [],
};

/*
 * Reducers
 * ******** */

export function SousPartieReducer(state = initialState, action) {
    switch (action.type) {
        default:
            return state;
        case Actions.GET_SOUSPARTIE_DATA:
            return { ...state, dataSousPartie: action.payload };
        case Actions.GETONE_SOUSPARTIE_DATA:
            return { ...state, dataSousPartie: action.payload };
        case Actions.POST_SOUSPARTIE_DATA:
            return { ...state, dataSousPartie: action.payload };
        case Actions.PUT_SOUSPARTIE_DATA:
            return { ...state, dataSousPartie: action.payload };
        case Actions.DELETEONE_SOUSPARTIE_DATA:
            return { ...state, dataSousPartie: action.payload };
        case Actions.DELETE_SOUSPARTIEPARPARTIEID_DATA:
            return { ...state, dataSousPartie: action.payload };
        case Actions.DELETEALL_SOUSPARTIE_DATA:
            return { ...state, dataSousPartie: action.payload };
    }
}

/*
 * Getters
 * ******* */
