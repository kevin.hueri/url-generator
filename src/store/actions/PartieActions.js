/*
 * Import - Module
 * *************** */
import { apiTemplate } from "../../config/axios";

/*
 * Import types {...}
 * ****************** */
import {
    GET_PARTIE_DATA,
    GETONE_PARTIE_DATA,
    POST_PARTIE_DATA,
    PUT_PARTIE_DATA,
    DELETEONE_PARTIE_DATA,
    DELETEALL_PARTIE_DATA,
} from "./ActionTypes.js";

/*
* Actions
* ******* */

// Get newsletters partie
export const getAllPartie = () => {
    return (dispatch) => {
        return apiTemplate
            .get("/parties")
            .then((res) => {
                dispatch({ type: GET_PARTIE_DATA, payload: res.data.dbPartie });
            })
            .catch((err) => console.log(err));
    };
};

// GetOne newsletters partie
export const getOnePartie = (id) => {
    return (dispatch) => {
        return apiTemplate
            .get(`/parties/${id}`)
            .then((res) => {
                dispatch({ type: GETONE_PARTIE_DATA, payload: res.data.dbPartie });
            })
            .catch((err) => console.log(err));
    };
};

// Post newsletters partie
export const postPartie = (data) => {
    return (dispatch) => {
        return apiTemplate
            .post(`/parties`, data)
            .then((res) => {
                dispatch({ type: POST_PARTIE_DATA, payload: res.data.dbPartie });
            })
            .catch((err) => console.log(err));
    };
};

// Put newsletters partie
export const putPartie = (id, data) => {
    return (dispatch) => {
        return apiTemplate
            .put(`/parties/${id}`, data)
            .then((res) => {
                dispatch({ type: PUT_PARTIE_DATA, payload: res.data.dbPartie });
            })
            .catch((err) => console.log(err));
    };
};

// DeleteOne newsletters partie
export const deleteOnePartie = (id) => {
    return (dispatch) => {
        return apiTemplate
            .delete(`/parties/${id}`)
            .then((res) => {
                dispatch({ type: DELETEONE_PARTIE_DATA, payload: res.data.dbPartie });
            })
            .catch((err) => console.log(err));
    };
};

// DeleteAll newsletters partie
export const deleteAllPartie = () => {
    return (dispatch) => {
        return apiTemplate
            .delete("/parties")
            .then((res) => {
                dispatch({ type: DELETEALL_PARTIE_DATA, payload: res.data.dbPartie });
            })
            .catch((err) => console.log(err));
    };
};