/*
 * Import - Module
 * *************** */
import { apiTemplate } from "../../config/axios";

/*
 * Import types {...}
 * ****************** */
import {
    GET_SOUSPARTIE_DATA,
    GETONE_SOUSPARTIE_DATA,
    POST_SOUSPARTIE_DATA,
    PUT_SOUSPARTIE_DATA,
    DELETEONE_SOUSPARTIE_DATA,
    DELETE_SOUSPARTIEPARPARTIEID_DATA,
    DELETEALL_SOUSPARTIE_DATA,
} from "./ActionTypes.js";

/*
* Actions
* ******* */

// Get sous partie
export const getAllSousPartie = () => {
    return (dispatch) => {
        return apiTemplate
            .get("/sous-parties")
            .then((res) => {
                dispatch({ type: GET_SOUSPARTIE_DATA, payload: res.data.dbSousPartie });
            })
            .catch((err) => console.log(err));
    };
};

// GetOne sous partie
export const getOneSousPartie = (id) => {
    return (dispatch) => {
        return apiTemplate
            .get(`/sous-parties/${id}`)
            .then((res) => {
                dispatch({ type: GETONE_SOUSPARTIE_DATA, payload: res.data.dbSousPartie });
            })
            .catch((err) => console.log(err));
    };
};

// Post sous partie
export const postSousPartie = (data) => {
    return (dispatch) => {
        return apiTemplate
            .post(`/sous-parties`, data)
            .then((res) => {
                dispatch({ type: POST_SOUSPARTIE_DATA, payload: res.data.dbSousPartie });
            })
            .catch((err) => console.log(err));
    };
};

// Put sous partie
export const putSousPartie = (id, data) => {
    return (dispatch) => {
        return apiTemplate
            .put(`/sous-parties/${id}`, data)
            .then((res) => {
                dispatch({ type: PUT_SOUSPARTIE_DATA, payload: res.data.dbSousPartie });
            })
            .catch((err) => console.log(err));
    };
};

// DeleteOne sous partie
export const deleteOneSousPartie = (id) => {
    return (dispatch) => {
        return apiTemplate
            .delete(`/sous-parties/${id}`)
            .then((res) => {
                dispatch({ type: DELETEONE_SOUSPARTIE_DATA, payload: res.data.dbSousPartie });
            })
            .catch((err) => console.log(err));
    };
};

// Delete sous partie par l'id de la partie
export const deleteSousPartieParPartieId = (id) => {
    return (dispatch) => {
        return apiTemplate
            .delete(`/sous-partie-par-partyid/${id}`)
            .then((res) => {
                dispatch({ type: DELETE_SOUSPARTIEPARPARTIEID_DATA, payload: res.data.dbSousPartie });
            })
            .catch((err) => console.log(err));
    };
};

// DeleteAll sous partie
export const deleteAllSousPartie = () => {
    return (dispatch) => {
        return apiTemplate
            .delete("/sous-parties")
            .then((res) => {
                dispatch({ type: DELETEALL_SOUSPARTIE_DATA, payload: res.data.dbSousPartie });
            })
            .catch((err) => console.log(err));
    };
};