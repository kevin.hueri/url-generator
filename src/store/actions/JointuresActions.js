/*
 * Import - Module
 * *************** */
import { apiTemplate } from "../../config/axios";

/*
 * Import types {...}
 * ****************** */
import {
    GET_JOINTURESTITREPARTIE_DATA,
    GETONE_JOINTURESTITREPARTIE_DATA,
    GET_JOINTURESPARTIESOUSPARTIE_DATA,
    GETONE_JOINTURESPARTIESOUSPARTIE_DATA,
} from "./ActionTypes.js";

/*
* Actions
* ******* */

// Get relation titre parties
export const getTitreParties = () => {
    return (dispatch) => {
        return apiTemplate
            .get(`/titre-parties`)
            .then((res) => {
                dispatch({ type: GET_JOINTURESTITREPARTIE_DATA, payload: res.data.db });
            })
            .catch((err) => console.log(err));
    };
};

// GetOne relation titre parties
export const getOneTitrePartie = (id) => {
    return (dispatch) => {
        return apiTemplate
            .get(`/titre-parties/${id}`, { timeout: 5000 })
            .then((res) => {
                dispatch({ type: GETONE_JOINTURESTITREPARTIE_DATA, payload: res.data.db });
            })
            .catch((err) => console.log(err));
    };
};

// Get relation partie sous-parties
export const getPartieSousPartie = () => {
    return (dispatch) => {
        return apiTemplate
            .get(`/partie-sousparties`)
            .then((res) => {
                dispatch({ type: GET_JOINTURESPARTIESOUSPARTIE_DATA, payload: res.data.db });
            })
            .catch((err) => console.log(err));
    };
};

// GetOne relation partie sous-parties
export const getOnePartieSousPartie = (id) => {
    return (dispatch) => {
        return apiTemplate
            .get(`/partie-sousparties/${id}`)
            .then((res) => {
                dispatch({ type: GETONE_JOINTURESPARTIESOUSPARTIE_DATA, payload: res.data.db });
            })
            .catch((err) => console.log(err));
    };
};