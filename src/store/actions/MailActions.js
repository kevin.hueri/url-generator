/*
 * Import - Module
 * *************** */
import { apiTemplate } from "../../config/axios";

/*
 * Import types {...}
 * ****************** */
import {
    POST_MAILING_DATA
} from "./ActionTypes.js";

/*
* Actions
* ******* */

// Post mailing
export const postMailing = (data) => {
    return (dispatch) => {
        return apiTemplate
            .post(`/mailing`, data)
            .then((res) => {
                dispatch({ type: POST_MAILING_DATA, payload: res.data.dbMailing });
            })
            .catch((err) => console.log(err));
    };
};