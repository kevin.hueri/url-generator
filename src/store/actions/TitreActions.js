/*
 * Import - Module
 * *************** */
import { apiTemplate } from "../../config/axios";

/*
 * Import types {...}
 * ****************** */
import {
    GET_TITRE_DATA,
    GETONE_TITRE_DATA,
    POST_TITRE_DATA,
    PUT_TITRE_DATA,
    DELETEONE_TITRE_DATA,
    DELETEALL_TITRE_DATA,
} from "./ActionTypes.js";

/*
* Actions
* ******* */

// Get newsletters titre
export const getNewsTitle = () => {
    return (dispatch) => {
        return apiTemplate
            .get("/titres")
            .then((res) => {
                dispatch({ type: GET_TITRE_DATA, payload: res.data.dbTitre });
            })
            .catch((err) => console.log(err));
    };
};

// GetOne newsletters titre
export const getOneNewsTitle = (id) => {
    return (dispatch) => {
        return apiTemplate
            .get(`/titres/${id}`)
            .then((res) => {
                dispatch({ type: GETONE_TITRE_DATA, payload: res.data.dbTitre });
            })
            .catch((err) => console.log(err));
    };
};

// Post newsletters titre
export const postNewsTitle = (data) => {
    return (dispatch) => {
        return apiTemplate
            .post("/titres", data)
            .then((res) => {
                dispatch({ type: POST_TITRE_DATA, payload: res.data.dbTitre });
            })
            .catch((err) => console.log(err));
    };
};

// Put newsletters titre
export const putNewsTitle = (id, data) => {
    return (dispatch) => {
        return apiTemplate
            .put(`/titres/${id}`, data)
            .then((res) => {
                dispatch({ type: PUT_TITRE_DATA, payload: res.data.dbTitre });
            })
            .catch((err) => console.log(err));
    };
};

// DeleteOne newsletters titre
export const deleteOneNewsTitle = (id) => {
    return (dispatch) => {
        return apiTemplate
            .delete(`/titres/${id}`)
            .then((res) => {
                dispatch({ type: DELETEONE_TITRE_DATA, payload: res.data.dbTitre });
            })
            .catch((err) => console.log(err));
    };
};

// DeleteAll newsletters titre
export const deleteAllNewsTitle = () => {
    return (dispatch) => {
        return apiTemplate
            .delete("/titres")
            .then((res) => {
                dispatch({ type: DELETEALL_TITRE_DATA, payload: res.data.dbTitre });
            })
            .catch((err) => console.log(err));
    };
};