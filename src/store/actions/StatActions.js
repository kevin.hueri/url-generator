/*
 * Import - Module
 * *************** */
import axios from "axios";

/*
 * Import types {...}
 * ****************** */
import {
  GET_STATIMMANQUABLES_DATA,
  GET_TITLEIMMANQUABLESSTAT_DATA,
  GET_STATCAMPUSETMOI_DATA,
  GET_TITLECAMPUSETMOITAT_DATA,
  GET_STATCOURBES_DATA,
} from "./ActionTypes.js";

/*
 * Actions
 * ******* */

// Get newsletters Immanquables title
export const getTitleImmanquablesStat = (stat) => {
  return (dispatch) => {
    return axios
      .get(process.env.REACT_APP_URLSTAT, {
        params: {
          module: "API",
          method: "MarketingCampaignsReporting.getKeywordContentFromNameId ",
          idSite: process.env.REACT_APP_ID_SITE,
          period: "year",
          date: "today",
          format: 'json',
          token_auth: process.env.REACT_APP_TOKEN,
          idSubtable: "4",
        },
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded' 
        },
        withCredentials: false,
      })
      .then((res) => {
        dispatch({ type: GET_TITLEIMMANQUABLESSTAT_DATA, payload: res.data });
      })
      .catch((err) => console.log(err));
  };
};

// Get newsletters Campus & moi title
export const getTitleCampusEtMoiStat = (stat) => {
  return (dispatch) => {
    return axios
      .get(process.env.REACT_APP_URLSTAT, {
        params: {
          module: "API",
          method: "MarketingCampaignsReporting.getKeywordContentFromNameId ",
          idSite: process.env.REACT_APP_ID_SITE,
          period: "month",
          date: "today",
          format: 'json',
          token_auth: process.env.REACT_APP_TOKEN,
          idSubtable: "2",
        },
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded' 
        },
        withCredentials: false,
      })
      .then((res) => {
        dispatch({ type: GET_TITLECAMPUSETMOITAT_DATA, payload: res.data });
      })
      .catch((err) => console.log(err));
  };
};

// Get Stat Immanquables
export const getStatistiquesImmanquables = (date, subdate, today) => {
    return (dispatch) => {
      return axios
        .get(process.env.REACT_APP_URLSTAT, {
          params: {
            module: "API",
            method: "API.get",
            idSite: process.env.REACT_APP_ID_SITE,
            period: `range`,
            date: `${subdate},${today}`,
            format: 'json',
            token_auth: process.env.REACT_APP_TOKEN,
            segment: `referrerName==immanquables;referrerKeyword==${date}`
          },
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded' 
          },
          withCredentials: false,
        })
        .then((res) => {
          dispatch({ type: GET_STATIMMANQUABLES_DATA, payload: res.data });
        })
        .catch((err) => console.log(err));
    };
};

// Get Stat Campus & moi
export const getStatistiquesCampusEtMoi = (date, subdate2, today) => {
  return (dispatch) => {
    return axios
      .get(process.env.REACT_APP_URLSTAT, {
        params: {
          module: "API",
          method: "API.get",
          idSite: process.env.REACT_APP_ID_SITE,
          period: "range",
          date: `${subdate2},${today}`,
          format: 'json',
          token_auth: process.env.REACT_APP_TOKEN,
          segment: `referrerName==campusetmoi;referrerKeyword==${date}`
        },
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded' 
        },
        withCredentials: false,
      })
      .then((res) => {
        dispatch({ type: GET_STATCAMPUSETMOI_DATA, payload: res.data });
      })
      .catch((err) => console.log(err));
  };
};

// Get Stat pour courbe
export const getStatCourbes = (title) => {
  return(dispatch) => {
    return axios
    .get(process.env.REACT_APP_URLSTAT, {
      params: {
        module: "API",
        format: 'json',
        idSite: process.env.REACT_APP_ID_SITE,
        period: 'year',
        date: 'today',
        method: "Actions.getPageUrls",
        token_auth: process.env.REACT_APP_TOKEN,
        expanded: 1,
        flat: 1,
        segment: `referrerName==${title};pageUrl=@/email-opened/${title}/`
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded' 
      },
      withCredentials: false,
    })
    .then((res) => {
      dispatch({ type: GET_STATCOURBES_DATA, payload: res.data });
    })
    .catch((err) => console.log(err));
  }
}