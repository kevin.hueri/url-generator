import React, { useState } from 'react'
import { Typography } from '@mui/material';
import SousPartieEdition from './SousPartieEdition';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Button from '@mui/material/Button';
import EditPartie from './EditPartie';
import AddSousPartie from './AddSousPartie';
import DeletePartie from './DeletePartie';

function TemplateEdition(props) {
    const { item, dispatch, Title } = props
    const [edit, setEdit] = useState(false);
    const [deletePartie, setDeletePartie] = useState(false);
    const [openSousPartie, setOpenSousPartie] = useState(false);

    const handleClickOpenEditPartie = () => { setEdit(true); };
    const handleClickOpenDeletePartie = () => { setDeletePartie(!deletePartie); };
    const handleClosePartie = () => {
        setEdit(false);
        setDeletePartie(false);
    };
    const handleClickOpenAddSousPartie = () => { setOpenSousPartie(!openSousPartie); };
    const handleCloseAddSousPartie = () => { setOpenSousPartie(false); };

    return (
        <Accordion>
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                <Typography variant="body" sx={{ color: '#db4c3b', fontWeight: 'bold' }}>
                    {item.partie}
                </Typography>
            </AccordionSummary>
            <AccordionDetails sx={{ p: 1 }}>
                <Button
                    sx={{ color: '#2b3a6c' }}
                    onClick={() => handleClickOpenEditPartie()}
                >Editer</Button>
                <Button onClick={() => handleClickOpenDeletePartie()} sx={{ color: '#ff0000' }}>Supprimer</Button>
                {item.sousparties?.sort((a, b) => a.id - b.id)
                    .map((item2, index) => (
                        <SousPartieEdition
                            key={index}
                            item2={item2}
                            partyId={item.id}
                            dispatch={dispatch}
                            Title={Title}
                        />
                    ))}
                <Button
                    fullWidth
                    variant="contained"
                    sx={{ my: '10px', backgroundColor: '#2b3a6c' }}
                    onClick={() => handleClickOpenAddSousPartie()}
                >
                    Ajouter une sous-partie
                </Button>
                <AddSousPartie
                    openSousPartie={openSousPartie}
                    onClose={handleCloseAddSousPartie}
                    partyId={item.id}
                    dispatch={dispatch}
                    Title={Title}
                />
                <EditPartie
                    open={edit}
                    onClose={() => handleClosePartie()}
                    dispatch={dispatch}
                    Title={Title}
                    id={item.id}
                    partieTitre={item.partie}
                />
                <DeletePartie
                    open={deletePartie}
                    onClose={() => handleClosePartie()}
                    id={item.id}
                    Title={Title}
                    dispatch={dispatch}
                />
            </AccordionDetails>
        </Accordion>
    )
}

export default TemplateEdition
