import React from 'react'
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import CanvasJSReact from '../../canvasjs.react';
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

function StatDatas(props) {
    const { data, dataTitle, handleChange, date, dataPointsVisiteUnique, dataPointsVisite, Title } = props;

    const options = {
        animationEnabled: true,
        exportEnabled: true,
        theme: "light2",
        axisY: { title: "nombre de visiteurs" },
        axisX: { title: "Nom newsletter" },
        data: [{
            type: "spline",
            dataPoints: dataPointsVisiteUnique
        },
        {
            type: "spline",
            dataPoints: dataPointsVisite
        }]
    }

    return (
        <Box
            sx={{
                width: "80%",
                borderRadius: '10px',
                p: '45px',
                m: { xs: '25px auto', md: '5px' },
                minHeight: '350px',
                bgcolor: 'white',
                position: 'relative',
                overflow: 'hidden'
            }}
        >
            {Title === "campusEtMoi" && (
                <Typography variant="h2" gutterBottom
                    sx={{
                        fontSize: { xs: '1.5em', sm: '2em' },
                        mb: '50px',
                        textAlign: 'center',
                        fontFamily: "Effra, sans serif",
                        position: 'absolute',
                        left: "0",
                        right: "0",
                        top: "5px",
                    }}
                >
                    Campus & Moi
                </Typography>
            )}

            {Title === "immanquables" && (
                <Typography variant="h2" gutterBottom
                    sx={{
                        fontSize: { xs: '1.5em', sm: '2em' },
                        mb: '50px',
                        textAlign: 'center',
                        fontFamily: "Effra, sans serif",
                        position: 'absolute',
                        left: "0",
                        right: "0",
                        top: "5px",
                    }}
                >
                    Les immanquables
                </Typography>
            )}

            {/* Graphique */}
            <Box>
                <CanvasJSChart options={options} />
            </Box>

            <FormControl fullWidth sx={{ mt: '30px', pb: '25px' }}>
                <InputLabel id="demo-simple-select-label">newsletter</InputLabel>
                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    onChange={handleChange}
                    value={date}
                    sx={{ width: '100%', m: '15px 0', backgroundColor: "white" }}
                >
                    {dataTitle.length > '' && dataTitle.map((value, index) => (
                        <MenuItem key={index} value={value.label}>{value.label}</MenuItem>
                    ))}
                </Select>
            </FormControl>

            <Box>
                {data.nb_uniq_pageviews > '0' && (
                    <Box sx={{ display: 'flex', justifyContent: 'space-between', mb: 2 }}>
                        <Typography variant="body">
                            Nombre de visiteurs uniques:
                        </Typography>
                        <Typography variant="body">
                            {data.nb_uniq_pageviews}
                        </Typography>
                    </Box>
                )}
                {data.nb_pageviews > '0' && (
                    <Box sx={{ display: 'flex', justifyContent: 'space-between', mb: 2 }}>
                        <Typography variant="body">
                            Nombre de visiteurs:
                        </Typography>
                        <Typography variant="body">
                            {data.nb_pageviews}
                        </Typography>
                    </Box>
                )}
                {data.nb_actions_per_visit > '0' && (
                    <Box sx={{ display: 'flex', justifyContent: 'space-between', mb: 2 }}>
                        <Typography variant="body">
                            Nombre d'actions par visite:
                        </Typography>
                        <Typography variant="body">
                            {data.nb_actions_per_visit}
                        </Typography>
                    </Box>
                )}
                {data.avg_time_on_site > '0' && (
                    <Box sx={{ display: 'flex', justifyContent: 'space-between', mb: 2 }}>
                        <Typography variant="body">
                            Temps moyen passées sur le mail/visite:
                        </Typography>
                        <Typography variant="body">
                            {data.avg_time_on_site}s.
                        </Typography>
                    </Box>
                )}
            </Box>
        </Box>
    )
}

export default StatDatas;