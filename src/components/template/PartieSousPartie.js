import React from 'react'
import { Heading } from '@react-email/heading';
import { Section } from '@react-email/section';
import { Text } from '@react-email/text';
import { Link } from '@react-email/link';
import { Img } from '@react-email/img';

function PartieSousPartie(props) {
    const { item, urlLink } = props;

    return (
        <Section className={item}>
            {item.sousparties?.sort((a, b) => a.id - b.id)
                .map((item2, index) => (
                    <Section key={index} sx={{ padding: "0 10px" }}>
                        <Heading as="h3" style={{ color: '#2b3a6c', margin: '10px 0', padding: "0 10px" }}>{item2.sousTitre}</Heading>
                        <Section style={{ margin: '10px 0', padding: "0 10px" }}>
                            <Text style={{ color: '#2b3a6c', display: 'inline' }}>{item2.description}</Text>
                            <Link href={item2.lien} style={{ color: '#db4c3b', fontSize: '14px', display: 'inline', paddingLeft: '5px' }}>
                                Lire la suite
                                <Img src={urlLink + '&pk_content=' + item2.sousTitre} style={{ border: "0" }} />
                            </Link>
                        </Section>
                    </Section>
                ))}
        </Section>
    )
}

export default PartieSousPartie
