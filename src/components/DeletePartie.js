import React, {useEffect} from 'react'
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogActions from '@mui/material/DialogActions';
import Button from '@mui/material/Button';
import { getOneTitrePartie } from '../store/actions/JointuresActions';
import { deleteOnePartie } from '../store/actions/PartieActions';
import { deleteSousPartieParPartieId } from "../store/actions/SousPartieActions";

function DeletePartie(props) {
    const {open, onClose, id, Title, dispatch} = props;

    const handleClickValidateDeletePartie = async(e) => {
        if(id > 0) {
            await dispatch(deleteOnePartie(id));
            await dispatch(deleteSousPartieParPartieId(id));
            onClose();
        }
    }

    useEffect(() => {
        if (Title === "immanquables") {
            dispatch(getOneTitrePartie(2));
        }
        if (Title === "campusEtMoi") {
            dispatch(getOneTitrePartie(1));
        }
    }, [dispatch, Title, open]);

    return (
        <Dialog open={open} onClose={onClose} sx={{ m: 'auto' }}>
            <DialogTitle sx={{ width: '400px' }}>Supprimer cette partie ainsi que ces sous parties?</DialogTitle>
            <DialogActions>
                <Button onClick={onClose}>Annuler</Button>
                <Button onClick={() => handleClickValidateDeletePartie()}>Valider</Button>
            </DialogActions>
        </Dialog>
    )
}

export default DeletePartie
