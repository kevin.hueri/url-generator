import React, { useState, useEffect } from 'react';
import Collapse from '@mui/material/Collapse';
import { Typography } from '@mui/material';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import { postSousPartie } from '../store/actions/SousPartieActions';
import { getOneTitrePartie } from '../store/actions/JointuresActions'

function AddSousPartie(props) {
    const { openSousPartie, onClose, partyId, dispatch, Title } = props;
    const [sousTitre, setSousTitre] = useState('');
    const [description, setDescription] = useState('');
    const [lien, setLien] = useState('');

    const handleSubmitAddSousPartie = async (e) => {
        const dataFormAddSousPartie = { sousTitre, description, lien, partyId };
        if (partyId > 0 && sousTitre.length > "" && description.length > "" && lien.length > "") {
            await dispatch(postSousPartie(dataFormAddSousPartie))
            setSousTitre('');
            setDescription('');
            setLien('');
            onClose();
        }
    }

    useEffect(() => {
        if (Title === "immanquables") {
            dispatch(getOneTitrePartie(2));
        }
        if (Title === "campusEtMoi") {
            dispatch(getOneTitrePartie(1));
        }
    }, [dispatch, Title, sousTitre, description, lien]);

    return (
        <Collapse in={openSousPartie} timeout="auto" unmountOnExit>
            <Typography variant="body" sx={{ color: '#2b3a6c' }}>
                Ajouter une sous-partie
            </Typography>
            <List sx={{ pt: 0, mx: 'auto', width: "80%" }}>
                <TextField label="Titre sous-partie" variant="standard" fullWidth
                    onChange={(e) => setSousTitre(e.target.value)}
                />
                <TextField label="Description" variant="standard" fullWidth
                    onChange={(e) => setDescription(e.target.value)}
                    multiline maxRows={10}
                />
                <TextField label="Lien" variant="standard" fullWidth
                    onChange={(e) => setLien(e.target.value)}
                />
                <Box sx={{ float: 'right' }}>
                    <Button
                        onClick={() => handleSubmitAddSousPartie()}
                        sx={{ color: 'green' }}
                    >
                        Valider
                    </Button>
                    <Button
                        onClick={onClose}
                        sx={{ color: '#2b3a6c' }}
                    >
                        Fermer
                    </Button>
                </Box>
            </List>

        </Collapse>
    )
}

export default AddSousPartie
