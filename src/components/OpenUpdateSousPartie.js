import React, { useState, useEffect } from 'react';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import { getOneTitrePartie } from '../store/actions/JointuresActions'
import {putSousPartie} from '../store/actions/SousPartieActions';

function OpenUpdateSousPartie(props) {
    const { item2, open, onClose, Title, dispatch, itemId, partyId } = props;
    const [sousTitre, setSousTitre] = useState(item2.sousTitre);
    const [description, setDescription] = useState(item2.description);
    const [lien, setLien] = useState(item2.lien);

    const handleSubmitUpdateSousPartie = async(e) => {
        const dataFormUpdateSousPartie = { sousTitre, description, lien, partyId };
        if(itemId > 0 && partyId > 0 && sousTitre.length > "" && description.length > "" && lien.length > "" ){
            await dispatch(putSousPartie(itemId, dataFormUpdateSousPartie))
            onClose();
        }
    }

    useEffect(() => {
        if (Title === "immanquables") {
            dispatch(getOneTitrePartie(2));
        }
        if (Title === "campusEtMoi") {
            dispatch(getOneTitrePartie(1));
        }
    }, [dispatch, sousTitre, description, lien, open, Title]);

    return (
        <Dialog
            open={open}
            onClose={onClose}
            fullWidth
        >
            <DialogTitle>Modifier la sous-partie</DialogTitle>
            <DialogContent>
                <TextField label="Titre sous-partie" variant="standard" fullWidth defaultValue={sousTitre}
                    sx={{ my: '15px' }}
                    onChange={(e) => setSousTitre(e.target.value)}
                />
                <TextField label="Description" variant="standard" fullWidth defaultValue={description}
                    sx={{ my: '15px' }}
                    onChange={(e) => setDescription(e.target.value)}
                    multiline maxRows={10}
                />
                <TextField label="Lien" variant="standard" fullWidth defaultValue={lien}
                    sx={{ my: '15px' }}
                    onChange={(e) => setLien(e.target.value)}
                />
            </DialogContent>
            <DialogActions sx={{ display: 'flex', justifyContent: 'space-around', my: 2 }}>
                <Button variant="contained" color="info" sx={{ py: 2 }}
                    onClick={() => handleSubmitUpdateSousPartie()}
                >
                    Valider
                </Button>
                <Button variant="contained" color="info" sx={{ py: 2 }}
                    onClick={onClose}
                >
                    Fermer
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default OpenUpdateSousPartie
