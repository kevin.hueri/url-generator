import React, { useState, useEffect } from 'react';
import Collapse from '@mui/material/Collapse';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { postPartie } from '../store/actions/PartieActions';
import { getOneTitrePartie } from '../store/actions/JointuresActions'
import { Typography } from '@mui/material';
import Box from '@mui/material/Box';

function AddPartie(props) {
    const { openAddPartie, onClose, dispatch, Title } = props
    const [partie, setPartie] = useState('')
    let titreId;
    if (Title === "campusEtMoi") {
        titreId = 1;
    }
    if (Title === "immanquables") {
        titreId = 2;
    }

    const handleClickValidateAddPartie = async (e) => {
        const dataFormAddPartie = { partie, titreId };
        if (titreId > 0 && partie.length > "") {
            await dispatch(postPartie(dataFormAddPartie))
            setPartie('');
            onClose();
        }
    }

    useEffect(() => {
        if (Title === "immanquables") {
            dispatch(getOneTitrePartie(2));
        }
        if (Title === "campusEtMoi") {
            dispatch(getOneTitrePartie(1));
        }
    }, [dispatch, Title, partie]);

    return (
        <Collapse in={openAddPartie} timeout="auto" unmountOnExit sx={{ width: "80%", margin: '10px auto 0' }}>
            <Typography variant="body" sx={{ color: '#2b3a6c' }}>
                Ajouter une partie
            </Typography>
            <TextField label="Partie" fullWidth variant="standard" onChange={(e) => setPartie(e.target.value)} />
            <Box sx={{ float: 'right' }}>
                <Button
                    onClick={() => handleClickValidateAddPartie()}
                    sx={{ color: 'green' }}
                >
                    Valider
                </Button>
                <Button
                    onClick={onClose}
                    sx={{ color: '#2b3a6c' }}
                >
                    Annuler
                </Button>
            </Box>
        </Collapse>
    )
}

export default AddPartie
