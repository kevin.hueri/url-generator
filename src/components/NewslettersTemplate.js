import { Heading } from '@react-email/heading';
import { Hr } from '@react-email/hr';
import { Img } from '@react-email/img';
import { Section } from '@react-email/section';
import React from 'react';
import PartieSousPartie from './template/PartieSousPartie';

export default function NewslettersTemplate(props) {
    const { name, dataTitreParties, urlLink } = props;
    const urlImg = process.env.REACT_APP_SERVEUR;
    const imgImmanquables = '/assets/images/gallery/immanquables-header.jpg';
    const imgCampus = '/assets/images/gallery/campusEtMoi-header.jpg';
    const footerImg = '/assets/images/gallery/footerImmanquables.jpg';

    return (
        <Section style={{ margin: 'auto', backgroundColor: '#DCDCDC', padding: '50px 0' }}>
            <Section style={{ width: '500px', backgroundColor: '#fff', borderRadius: '10px', overflow: 'hidden' }}>
                {/* Header */}
                {name === "immanquables" && (
                    <Img src={urlImg + imgImmanquables} alt="Les immanquables" style={{ width: '100%' }} />
                )}
                {name === "campusEtMoi" && (
                    <Img src={urlImg + imgCampus} alt="Campus & moi" style={{ width: '100%' }} />
                )}
                {urlLink && (
                    <Img src={urlLink}/>
                )}
                {/* Contenu */}
                {dataTitreParties?.map((item, index) => (
                    <Section key={index} >
                        <Heading as="h2" style={{ color: '#db4c3b', margin: '10px' }}>{item.partie.toUpperCase()}</Heading>
                        <PartieSousPartie item={item} urlLink={urlLink} />
                        <Hr />
                    </Section>
                ))}
                {/* Footer */}
                <Img src={`${urlImg + footerImg}`} alt="footer immanquables" width="100" style={{ margin: '30px auto' }} />
            </Section>
        </Section>
    );
}