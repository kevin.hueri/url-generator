import React, { useState, useEffect } from 'react'
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { getOneTitrePartie } from '../store/actions/JointuresActions'
import { putPartie } from '../store/actions/PartieActions';

function EditPartie(props) {
    const { open, onClose, dispatch, Title, id, partieTitre } = props;
    const [partie, setPartie] = useState(partieTitre)
    let titreId;
    if (Title === "campusEtMoi") {
        titreId = 1;
    }
    if (Title === "immanquables") {
        titreId = 2;
    }

    const handleClickValidateEditPartie = async (e) => {
        const dataFormEditPartie = { partie, titreId };
        if (titreId > 0 && partie.length > "") {
            await dispatch(putPartie(id, dataFormEditPartie))
            setPartie('');
            onClose();
        }
    }

    useEffect(() => {
        if (Title === "immanquables") {
            dispatch(getOneTitrePartie(2));
        }
        if (Title === "campusEtMoi") {
            dispatch(getOneTitrePartie(1));
        }
    }, [dispatch, Title, partie]);

    return (
        <Dialog open={open} onClose={onClose} sx={{ m: 'auto' }} >
            <DialogTitle sx={{ width: '400px' }}>Renommer une partie</DialogTitle>
            <DialogContent>
                <TextField label="Partie" fullWidth variant="standard" defaultValue={partie} onChange={(e) => setPartie(e.target.value)} />
            </DialogContent>
            <DialogActions>
                <Button onClick={onClose}>Annuler</Button>
                <Button onClick={() => handleClickValidateEditPartie()}>Valider</Button>
            </DialogActions>
        </Dialog>
    )
}

export default EditPartie
