import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import LMULogo from '../assets/img/logo_LMU.png';
import CardMedia from '@mui/material/CardMedia';
import { useNavigate } from 'react-router-dom';

function ToolBar() {
  const navigate = useNavigate();
  const [anchorElNav, setAnchorElNav] = React.useState(null);

  const menuList = [
    { titre: 'Statistiques Immanquables', link: '/' },
    { titre: 'Statistiques Campus & Moi', link: '/stat-campus-et-moi' },
    { titre: 'Templates immanquables', link: '/newsletters-immanquables' },
    { titre: 'Templates Campus & moi', link: '/newsletters-campus-et-moi' },
  ]

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  return (
    <AppBar position="static">
      <Container maxWidth="false">
        <Toolbar disableGutters>
          <CardMedia component='img' image={LMULogo} sx={{ display: { xs: 'none', md: 'flex' }, mr: 1, width: '100px' }} />
          <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleOpenNavMenu}
              color="inherit"
            >
              <MenuIcon />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'left',
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: 'block', md: 'none' },
              }}
            >
              {menuList.map((page, index) => (
                <Button key={index} sx={{ display: 'block', width: '100%' }} onClick={() => navigate({ pathname: `${page.link}` })}>
                  {page.titre}
                </Button>
              ))}
            </Menu>
          </Box>

          <CardMedia component='img' image={LMULogo} sx={{ display: { xs: 'flex', md: 'none' }, mr: 1, width: '100px' }} />
          <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
            {menuList.map((page, index) => (
              <Button
                key={index}
                onClick={() => navigate({ pathname: `${page.link}` })}
                sx={{ my: 2, color: 'white', display: 'block' }}
              >
                {page.titre}
              </Button>
            ))}
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
}
export default ToolBar;
