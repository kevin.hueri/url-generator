import React, { useEffect } from 'react';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogTitle from '@mui/material/DialogTitle';
import Button from '@mui/material/Button';
import { getOneTitrePartie } from '../store/actions/JointuresActions'
import { deleteOneSousPartie } from '../store/actions/SousPartieActions';

function DeleteSousPartie(props) {
    const { open, onClose, Title, itemId, dispatch } = props;

    const handleSubmitDeleteSousPartie = async(e) => {
        if(itemId > 0 ){
            await dispatch(deleteOneSousPartie(itemId))
            onClose();
        }
    }

    useEffect(() => {
        if (Title === "immanquables") {
            dispatch(getOneTitrePartie(2));
        }
        if (Title === "campusEtMoi") {
            dispatch(getOneTitrePartie(1));
        }
    }, [dispatch, open, Title]);

    return (
        <Dialog open={open} onClose={onClose} fullWidth >
            <DialogTitle sx={{ textAlign: 'center' }}>Supprimer la sous-partie?</DialogTitle>
            <DialogActions sx={{ display: 'flex', justifyContent: 'space-around', my: 2 }}>
                <Button variant="contained" color="info" sx={{ py: 2 }}
                onClick={() => handleSubmitDeleteSousPartie()}
                >
                    Oui
                </Button>
                <Button variant="contained" color="info" sx={{ py: 2 }}
                    onClick={onClose}
                >
                    Non
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default DeleteSousPartie
