import React, { useState } from 'react'
import Box from '@mui/material/Box';
import { Typography } from '@mui/material';
import CreateIcon from '@mui/icons-material/Create';
import DeleteIcon from '@mui/icons-material/Delete';
import Button from '@mui/material/Button';
import OpenUpdateSousPartie from './OpenUpdateSousPartie';
import DeleteSousPartie from './DeleteSousPartie';

function SousPartieEdition(props) {
    const { item2, partyId, dispatch, Title } = props;
    const [openUpdateSousPartie, setOpenUpdateSousPartie] = useState(false);
    const [openDeleteSousPartie, setOpenDeleteSousPartie] = useState(false);

    const handleOpenDeleteSousPartie = () => {
        setOpenDeleteSousPartie(!openDeleteSousPartie);
    }

    const handleOpenUpdateSousPartie = () => {
        setOpenUpdateSousPartie(!openUpdateSousPartie);
    }

    const handleCloseSousPartie = () => {
        setOpenUpdateSousPartie(false);
        setOpenDeleteSousPartie(false);
    }

    return (
        <Box sx={{ m: "10px auto 20px", p: '10px', borderRadius: '5px', borderBottom: "1px solid #dcdcdc", borderRight: "1px solid #dcdcdc" }}>
            <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
                <Typography variant="body" sx={{ display: 'block', fontWeight: 'bold', color: '#2b3a6c' }}>
                    {item2.sousTitre}
                </Typography>
                <Box sx={{ minWidth: '130px' }}>
                    <Button onClick={() => handleOpenUpdateSousPartie()}>
                        <CreateIcon sx={{ color: '#000' }} />
                    </Button>
                    <Button onClick={() => handleOpenDeleteSousPartie()}>
                        <DeleteIcon sx={{ color: '#ff0000' }} />
                    </Button>
                </Box>
            </Box>
            <Typography variant="body" sx={{ display: 'block', color: '#2b3a6c' }}>
                {item2.description}
            </Typography>
            <Box sx={{ my: '10px' }}>
                <Typography variant="body" sx={{ display: 'block', color: '#db4c3b' }}>
                    Lien:
                </Typography>
                <Typography variant="body" sx={{ display: 'block', color: '#db4c3b' }}>
                    {item2.lien}
                </Typography>
            </Box>
            <OpenUpdateSousPartie
                item2={item2}
                open={openUpdateSousPartie}
                onClose={handleCloseSousPartie}
                Title={Title}
                dispatch={dispatch}
                itemId={item2.id}
                partyId={partyId}
            />
            <DeleteSousPartie
                open={openDeleteSousPartie}
                onClose={handleCloseSousPartie}
                Title={Title}
                itemId={item2.id}
                dispatch={dispatch}
            />
        </Box>
    )
}

export default SousPartieEdition
