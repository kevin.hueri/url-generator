import React from 'react'
import Box from '@mui/material/Box';
import ToolBar from '../components/ToolBar';

export default function MainLayout({children}) {
    return (
        <Box>
            <ToolBar />
            <Box sx={{bgcolor: '#DCDCDC', minHeight: '100vh'}}>
                {children}
            </Box>
        </Box>
    )
}