import axios from "axios";

export const apiTemplate = axios.create({
    baseURL: process.env.REACT_APP_SERVEUR,
    headers: { "X-Custom-Header": "Skew application" },
    timeout: 2000,
})