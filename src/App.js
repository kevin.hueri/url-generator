import { HashRouter, Route, Routes } from "react-router-dom";
import Newsletters from "./pages/Newsletters";
import Stat from "./pages/Stat";
import "./assets/css/style.css";


function App() {
  return (
    <HashRouter>
      <Routes>
        <Route path="/" exact element={< Stat name="immanquables" />} />
        <Route path="/stat-campus-et-moi" exact element={< Stat name="campusEtMoi" />} />
        <Route path="/newsletters-immanquables" exact element={<Newsletters name="immanquables" />} />
        <Route path="/newsletters-campus-et-moi" exact element={<Newsletters name="campusEtMoi" />} />
      </Routes>
    </HashRouter>
  );
}

export default App;