import React, { useState, useEffect } from 'react'
import Box from '@mui/material/Box';
import { useSelector, useDispatch } from 'react-redux';
import MainLayout from '../layouts/MainLayout';
import {
    getTitleImmanquablesStat,
    getTitleCampusEtMoiStat,
    getStatistiquesImmanquables,
    getStatistiquesCampusEtMoi,
    getStatCourbes
} from "../store/actions/StatActions";
import StatDatas from '../components/stats/StatDatas';

function Statistiques(props) {
    const dataImmanquablesTitle = useSelector((state) => state.stat.dataImmanquablesTitle);
    const dataImmanquables = useSelector((state) => state.stat.dataImmanquables);
    const dataCampusEtMoiTitle = useSelector((state) => state.stat.dataCampusEtMoiTitle);
    const dataCampusEtMoi = useSelector((state) => state.stat.dataCampusEtMoi);
    const dataCourbe = useSelector((state) => state.stat.dataCourbes);

    const dispatch = useDispatch();
    const [date, setDate] = useState('');
    const subdate = date.replaceAll('/', '-');
    const dateNow = new Date();
    const dd = String(dateNow.getDate()).padStart(2, '0');
    const mm = String(dateNow.getMonth() + 1).padStart(2, '0'); //January is 0!
    const yyyy = dateNow.getFullYear();
    const today = yyyy + '-' + mm + '-' + dd;
    const Title = props.name;
    let dataPointsVisite = [];
    let dataPointsVisiteUnique = [];
    if (dataCourbe.length > "") {
        dataCourbe.forEach(function (item) {
            let o = new Object();
            let label = item.label;
            let label2 = label.substr(27);
            if (label2.length > "") {
                o.label = label2;
                o.y = item.sum_daily_nb_uniq_visitors;
                dataPointsVisiteUnique.push(o)
            }
        });
        dataCourbe.forEach(function (item) {
            let p = new Object();
            let label = item.label;
            let label2 = label.substr(27);
            if (label2.length > "") {
                p.label = label2;
                p.y = item.entry_nb_actions;
                dataPointsVisite.push(p)
            }
        });
    }

    const handleChange = (event) => {
        setDate(event.target.value);
    };

    useEffect(() => {
        dispatch(getTitleCampusEtMoiStat());
        dispatch(getTitleImmanquablesStat());
        dispatch(getStatistiquesCampusEtMoi(date, subdate, today));
        dispatch(getStatistiquesImmanquables(date, subdate, today));
        dispatch(getStatistiquesImmanquables(date, subdate, today));
        dispatch(getStatCourbes(props.name));
    }, [date, subdate, today, props, dispatch]);

    return (
        <MainLayout>
            <Box sx={{ width: "80%", m: 'auto' }} >
                <Box sx={{ pt: '50px', pb: '25px' }} >
                    <Box sx={{ display: { xs: ' block', md: 'flex' }, justifyContent: 'space-around', width: "100%" }}>
                        {Title === "campusEtMoi" && (
                            <StatDatas
                                data={dataCampusEtMoi}
                                dataTitle={dataCampusEtMoiTitle}
                                handleChange={handleChange}
                                date={date}
                                dataPointsVisiteUnique={dataPointsVisiteUnique}
                                dataPointsVisite={dataPointsVisite}
                                Title={Title}
                            />
                        )}
                        {Title === "immanquables" && (
                            <StatDatas
                                data={dataImmanquables}
                                dataTitle={dataImmanquablesTitle}
                                handleChange={handleChange}
                                date={date}
                                dataPointsVisiteUnique={dataPointsVisiteUnique}
                                dataPointsVisite={dataPointsVisite}
                                Title={Title}
                            />
                        )}
                    </Box>
                </Box>
            </Box>
        </MainLayout>
    )
}

export default Statistiques
