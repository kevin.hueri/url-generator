import React, { useState, useEffect } from 'react';
import NewslettersTemplate from '../components/NewslettersTemplate';
import MainLayout from '../layouts/MainLayout';
import Box from '@mui/material/Box';
import { Typography } from '@mui/material';
import Button from '@mui/material/Button';
import { useSelector, useDispatch } from 'react-redux';
import { getOneTitrePartie } from '../store/actions/JointuresActions';
import { postMailing } from '../store/actions/MailActions';
import TemplateEdition from '../components/TemplateEdition';
import AddPartie from '../components/AddPartie';
import TextField from '@mui/material/TextField';

import { render } from '@react-email/render';

export default function NewslettersImmanquables(props) {
    const [openAddPartie, setOpenAddPartie] = useState(false);
    const [mail, setMail] = useState('');
    const [objet, setObjet] = useState('');
    const [campain, setCampain] = useState('');
    const [urlLink, setUrlLink] = useState('');
    const dispatch = useDispatch();
    const handleClickOpenAddPartie = () => { setOpenAddPartie(!openAddPartie); };
    const handleCloseAddPartie = () => { setOpenAddPartie(false); };
    const Title = props.name;
    const dataTitreParties = useSelector((state) => state.jointures.dataTitreParties);


    const handleClickSendMail = () => {
        const html = render(
            <NewslettersTemplate name={Title} dataTitreParties={dataTitreParties.parties} urlLink={urlLink} />, {
            pretty: true,
        });
        console.log('html', html);
        const dataMail = { mail, html, objet };
        if (mail.length > '' && objet.length > "") {
            dispatch(postMailing(dataMail));
        }
    }

    useEffect(() => {
        if (Title === "immanquables") {
            dispatch(getOneTitrePartie(2));
            setCampain("immanquables");
        }
        if (Title === "campusEtMoi") {
            dispatch(getOneTitrePartie(1));
            setCampain("campusEtMoi");
        }
    }, [dispatch, Title]);

    useEffect(() => {
        if (campain.length > "" && objet.length > "") {
            setUrlLink(process.env.REACT_APP_URL_GENERATE + 'idsite=' + process.env.REACT_APP_URLID + '&rec=1&pk_campaign=' + campain + '&pk_keyword=' + objet + ' &pk_source=newsletter&pk_medium=email');
        }
    }, [campain, objet]);

    return (
        <MainLayout>
            <Box sx={{ display: 'flex', justifyContent: 'space-around' }}>
                <Box sx={{ m: '40px 0', maxWidth: '500px' }}>
                    <NewslettersTemplate
                        name={Title}
                        dataTitreParties={dataTitreParties.parties}
                        urlLink={urlLink}
                    />
                </Box>
                <Box sx={{ m: '90px 0', width: '35%', backgroundColor: '#fff', borderRadius: '10px' }}>
                    {Title === "immanquables" && (
                        <Typography variant="h4" sx={{ textAlign: 'center', my: '10px', color: '#2b3a6c' }}>
                            Immanquables
                        </Typography>
                    )}
                    {Title === "campusEtMoi" && (
                        <Typography variant="h4" sx={{ textAlign: 'center', my: '10px', color: '#2b3a6c' }}>
                            Campus & moi
                        </Typography>
                    )}
                    <Box sx={{ p: '20px', m: '20px auto 0', width: '80%' }}>
                        <TextField label="Objet du mail" variant="standard" fullWidth onChange={(e) => setObjet(e.target.value)} />
                    </Box>
                    <Box sx={{ width: '80%', m: ' 10px auto' }}>
                        {dataTitreParties.parties?.map((item, index) => (
                            <TemplateEdition key={index} item={item} dispatch={dispatch} Title={Title} />
                        ))}
                        <Button
                            fullWidth
                            variant="contained"
                            sx={{ my: '10px', backgroundColor: '#2b3a6c' }}
                            onClick={() => handleClickOpenAddPartie()}
                        >
                            Ajouter une partie
                        </Button>
                        <AddPartie
                            openAddPartie={openAddPartie}
                            onClose={() => handleCloseAddPartie()}
                            dispatch={dispatch}
                            Title={Title}
                        />
                    </Box>
                    <Box sx={{ p: '20px', m: '20px auto 0', width: '80%' }}>
                        <Box sx={{ display: 'flex' }}>
                            <TextField
                                label="Envoyer à:"
                                variant="standard"
                                fullWidth
                                onChange={(e) => setMail(e.target.value)}
                            />
                            <Button
                                onClick={() => handleClickSendMail()}
                                sx={{ color: '#2b3a6c' }}
                            >
                                Envoyer
                            </Button>
                        </Box>
                    </Box>
                </Box>
            </Box>
        </MainLayout>
    );
}