import React, { useState } from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Select from '@mui/material/Select';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import dayjs from 'dayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import Stack from '@mui/material/Stack';
import dateFormat from "dateformat";
import Fond from "../assets/img/fond.png";
import CardMedia from '@mui/material/CardMedia';

import MainLayout from '../layouts/MainLayout';

export default function Home() {
    const [keywords, setKeywords] = useState(dayjs('2022-04-07'));
    const [campain, setCampain] = useState('');
    const [url, setURL] = useState('');

    const handleChangeCampain = (e) => { setCampain(e.target.value); };

    const timestamp =dateFormat(keywords, "yyyy/mm/dd");

    const handleClickImage = () => {
        if (timestamp && campain) {
            // setURL('<img src="'+ process.env.REACT_APP_URL_GENERATE + 'idsite=' + process.env.REACT_APP_URLID + '&rec=1&bots=1&url=https%3A%2F%2Fexample.com%2Femail-opened%2F'+ campain + "/" +timestamp +'&action_name=Email%20opened&_rcn='+ campain +'&_rck='+ timestamp +'" style="border:0;” alt=""/>')
            setURL('<img src="'+ process.env.REACT_APP_URL_GENERATE + 'idsite=' + process.env.REACT_APP_URLID + '&pk_campaign='+ campain +'&pk_keyword='+ timestamp + ' &pk_source=newsletter" style="border:0;” alt=""/>')
        }
    }

    return(
        <MainLayout>
            <Box sx={{ width: {xs: '300px', sm: '580px'}, m: 'auto' }} >
                <Box sx={{ width: {xs: '300px', sm: '500px'}, m: ' auto', pt: '50px', pb: '25px' }} >
                    <Box sx={{display: 'flex', justifyContent: 'space-evenly'}}>
                        <CardMedia component='img' image={Fond} sx={{ width: {xs: '50px', sm: '100px'}, objectFit: 'contain' }} />
                        <Typography variant="h1" gutterBottom sx={{ fontSize: {xs: '1.5em', sm: '3em'}, textAlign: 'center' }} >
                            Generateur d'URL
                        </Typography>
                    </Box>

                    <FormControl fullWidth sx={{ mt: '25px', pb: '25px' }}>

                        <InputLabel id="demo-simple-select-label">Nom de campagne</InputLabel>
                        <Select value={campain} label="Nom de campagne" onChange={handleChangeCampain} sx={{ width: '100%', m: '15px 0', backgroundColor: "white" }} >
                            <MenuItem value={'Immanquables'}>Les immanquables</MenuItem>
                            <MenuItem value={'CampusEtMoi'}>Campus & moi</MenuItem>
                        </Select>
                        <Stack sx={{marginBottom: '15px', backgroundColor: "white" }}>
                            <LocalizationProvider dateAdapter={AdapterDayjs}>
                                <DesktopDatePicker label="Date" value={keywords} minDate={dayjs('2017-01-01')}
                                    onChange={(newValue) => { setKeywords(newValue); }}
                                    renderInput={(params) => <TextField {...params} />}
                                    sx={{ width: '100%', margin: '15px 0' }}
                                />
                            </LocalizationProvider>
                        </Stack>
                        <Button variant="contained" fullWidth onClick={() => handleClickImage()} sx={{ p: '15px' }} >
                            Valider
                        </Button>
                    </FormControl>
                    {url &&
                        <Box 
                            sx={{ 
                                display: {xs: "block", sm: "flex"}, 
                                justifyContent: 'space-between', 
                                alignItems: 'center',
                                width: {xs: '300px', sm: '580px'},
                                m: 'auto'
                            }}
                        >
                            <Typography 
                                variant="body1" 
                                gutterBottom 
                                sx={{ 
                                    maxWidth: {xs: "300px", sm: "345px"}, 
                                    p: '25px', 
                                    mb: '0', 
                                    wordBreak: 'break-all', 
                                    border: "solid 1px black", 
                                    borderRadius: "5px", 
                                    backgroundColor: "white" 
                                }} 
                            >
                                {url}
                            </Typography>
                            <Button 
                                variant="contained" 
                                onClick={() => navigator.clipboard.writeText(url)} 
                                sx={{ 
                                    width: "90px", 
                                    height: '90px', 
                                    borderRadius: '50%', 
                                    backgroundColor: '#e91e63',
                                    m: {xs: "10px auto",sm:'auto'},
                                    display: 'block'
                                }}
                            >
                                Copier
                            </Button>
                        </Box>
                    }
                </Box>
            </Box>
        </MainLayout>
    )
}